require("dotenv").config()
const API_URL = "https://sepolia.infura.io/v3/e22263da932241b4a652df190751bfc0"

const ACCOUNT = "911510851e683067cbd3a0ed25d484dddfb829c537f272df53342098c3a04763"

const PRIVATE_KEY = "A50DAf4AE11029E1ca41F40452E61E2Df5BDFA96"

const { createAlchemyWeb3 } = require("@alch/alchemy-web3")
const web3 = createAlchemyWeb3(API_URL)

const contract = require("../artifacts/contracts/MyNFT.sol/MyNFT.json")
const contractAddress = "0xf536511E0Cf7B0dddc1FC21871aef749bBB724F3"
const nftContract = new web3.eth.Contract(contract.abi, contractAddress)

export async function mintNFT(tokenURI) {
  // const accountNonce =(web3.eth.getTransactionCount(PUBLIC_KEY) + 1).toString(16)
  // const nonce = await web3.eth.getTransactionCount(PUBLIC_KEY, "latest") //get latest nonce
  const nonce = await web3.eth.getTransactionCount(ACCOUNT, "latest") + 4 //get latest nonce

  //the transaction
  const tx = {
    from: ACCOUNT,
    to: contractAddress,
    nonce: nonce,
    gas: 500000,
    data: nftContract.methods.mintNFT(ACCOUNT, tokenURI).encodeABI(),
  }

  const signPromise = web3.eth.accounts.signTransaction(tx, PRIVATE_KEY)
  signPromise
    .then((signedTx) => {
      web3.eth.sendSignedTransaction(
        signedTx.rawTransaction,
        function (err, hash) {
          if (!err) {
            console.log(
              "The hash of your transaction is: ",
              hash,
              "\nCheck Alchemy's Mempool to view the status of your transaction!"
            )
          } else {
            console.log(
              "Something went wrong when submitting your transaction:",
              err
            )
          }
        }
      )
    })
    .catch((err) => {
      console.log(" Promise failed:", err)
    })
}

// ipfs of the NFT-metadata.json
mintNFT("ipfs://QmRGgL8TGdMuDtgNynXp43K5d8oZbMmyVde1fGEook1jcb")



import { createRouter, createWebHistory } from 'vue-router'
import BatteryListView from '../views/BatteryListView.vue'
import BatteryEdit from '../views/battery/Edit.vue'
import BatteryRegister from '../views/battery/Register.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/:pathMatch(.*)*',
      name: 'not-found',
      component: () => import('../views/NotFoundView.vue'),
      props: {
        // showExtra: true
        showExtra: false
      }
    },
    {
      path: '/',
      name: 'BatteryList',
      component: BatteryListView,
      // props: (route) => ({ page: parseInt(route.query.page) || 1 })
    },
    {
      path: '/add',
      name: 'BatteryAdd',
      component: () => import('../views/BatteryAddView.vue')
      // props: (route) => ({ page: parseInt(route.query.page) || 1 })
    },
    {
      path: '/batteries/:id',
      name: 'BatteryLayout',
      // props: true,
      props: (route) => ({
        id: route.params.id,
        showExtra: route.query.e
      }),
      component: () => import('../views/battery/Layout.vue'),
      children: [
        {
          path: '',
          name: 'BatteryDetails',
          component: () => import('../views/battery/Details.vue')
        },
        {
          path: 'register',
          name: 'BatteryRegister',
          component: BatteryRegister
        },
        {
          path: 'register',
          name: 'BatteryMintCertificate',
          component: () => import('../views/battery/Mint.vue')
        },
        {
          path: 'edit',
          name: 'BatteryEdit',
          component: BatteryEdit
        }
      ]
    },
    {
      path: '/about',
      name: 'Stats',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/StatsView.vue')
    }
  ]
})

export default router
